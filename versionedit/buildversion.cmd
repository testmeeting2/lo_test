@echo off

rem buildversion.hのパスを指定する
set HEADERFILE=%~dp0buildversion.h

rem ビルド日の指定
set BUILDYEAR=%date:~-10,4%
set BUILDDATE=%date:~-10,4%%date:~-5,2%%date:~-2,2%

rem 定義文字列の作成
set PREFIX_VERSION=xxx version : 1.4.7.
set PREFIX_COPYRIGHT=Copyright (C) :

set VERSION=%PREFIX_VERSION%%BUILDDATE%
set COPYRIGHT=%PREFIX_COPYRIGHT%%BUILDYEAR%

rem ファイルへの書き込み
echo // buildversion.h > "%HEADERFILE%"
echo // this file is output during the build. > "%HEADERFILE%"
echo // do not touch(edit).!! > "%HEADERFILE%"
echo. >> "%HEADERFILE%"
echo #pragma once >> "%HEADERFILE%"
echo. >> "%HEADERFILE%"
echo #define ABOUT_BUILDVERSION    "%VERSION%" >> "%HEADERFILE%"
echo #define ABOUT_COPYRIGHT       "%COPYRIGHT%" >> "%HEADERFILE%"
